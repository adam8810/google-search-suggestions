<html>
    <head>
        <title>Suggestions</title>

        <script type="text/javascript" src="assets/javascript/jquery.js"></script>
        
        <script type="text/javascript">
            $('document').ready(function()
            {
                text_prompt = 'Enter search';
                
                // Add initial text to textbox
                if($('#tx').val() == '')
                {
                    $('#tx').val(text_prompt)
                }
                
                // When clicked remove text
                $('#tx').click(function(){
                    if($('#tx').val() == '' || $('#tx').val() == text_prompt)
                    {
                        $('#tx').val('')
                    }
                })
                
                // When text input loses focus
                $('#tx').blur(function(){
                    if($('#tx').val() == '')
                    {
                        $('#tx').val(text_prompt)
                    } 
                })
                
                
                $('#tx').keyup(function()
                {    
                    $('#search_dd').html(get_json($('#tx').val()));                 
                })
                
                function get_json(text)
                {
                    // To avoid sending an empty query
                    if(text != '')
                    {
                        $('#search_dd').slideDown('fast', function(){
                            query = encodeURI(text);
                            url = 'http://dev.ambooth.com/demo/suggest/connection.php?q='  + text;
                    
                            $.get(url, function(data) {
                                $('#search_dd').html(data)
                            });
                        })
                
                    }
                    else
                    {
                        $('#search_dd').slideUp('fast', function(){
                            $('#search_dd').html('');
                        })
                    }
                }
                
                function hi_mal(text)
                {
                    $('#mal').html(text)
                }
            });
        </script>

        <style>
            div#search_dd{
                background: #FFF;
                border: 1px solid gray;
                border-top: none;
                display: none;
                width: 200px;
                height: 100px;
                overflow-y: scroll;
            }

            label{
                font-size: 12px;
                font-style: italic;
            }

            div#mal{
                background: purple;
                color: yellow;
                width: 200px;
                text-align: center;
            }

            form{
                width: 200px;
                margin: 20px auto;
            }

            input#tx{
                width: 200px;
            }

            li{
                list-style: none;
                padding: 2px 2px 2px 10px;
            }
            .odd{
                background: #eee;
            }
        </style>
    </head>
    <body>
        <form>
            <label>No need to press enter :)</label>
            <input id="tx"/>
            <div id="search_dd"></div>
            <div id="mal"></div>
        </form>
    </body>
</html>